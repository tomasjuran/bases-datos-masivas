#!/usr/bin/env python3
""" Toma un archivo en formato .arff 
	y elabora el Decision Tree correspondiente """
import sys
import subprocess
import argparse
from math import log

import matplotlib.pyplot as plt
import networkx as nx

import pydot
from networkx.drawing.nx_pydot import write_dot

tree = nx.DiGraph()
global_id = -1

def get_global_id():
	global global_id
	global_id += 1
	return global_id

def create_node(label):
	global tree
	node_id = get_global_id()
	tree.add_node(node_id, label=label)
#	print("added node: " + str(node_id) + "; label: " + label)
	return node_id

def add_edge(node1, node2, label):
	global tree
	tree.add_edge(node1, node2, label=label)
#	print("added edge: " + str(node1) + " -> " + str(node2) + "; label: " + label)

def entropy(proportions):
	entropy = 0
	for proportion in proportions:
		if (proportion > 0):
			entropy -= proportion * log(proportion, 2)
	return entropy

def gain(feature_freqs):
	fclass_freqs = {}
	for feature_value_freqs in feature_freqs.values():
		for fclass, fclass_freq in feature_value_freqs.items():
			if not fclass in fclass_freqs:
				fclass_freqs[fclass] = fclass_freq
			else:
				fclass_freqs[fclass] += fclass_freq
	total_instances = sum(fclass_freqs.values())
	fclass_proportions = [x/total_instances for x in fclass_freqs.values()]
	gain = entropy(fclass_proportions)

	for feature_value, feature_value_freqs in feature_freqs.items():
		feature_proportions = [x/total_instances for x in feature_value_freqs.values()]
		feature_value_instances = sum(feature_value_freqs.values())
		gain -= (feature_value_instances / total_instances) * entropy(feature_proportions)

	return gain

def max_gain(instances, fclass, features):
	# Almacenar las frecuencias
	frequencies = {}
	for instance in instances:
		for feature in features:
			# Si no está el feature
			if feature not in frequencies:
				frequencies[feature] = {}
			# Si no está el valor del feature
			if instance[feature] not in frequencies[feature]:
				frequencies[feature][instance[feature]] = {}
			# Si no está el valor de la clase para el feature
			if instance[fclass] not in frequencies[feature][instance[feature]]:
				frequencies[feature][instance[feature]][instance[fclass]] = 0

			frequencies[feature][instance[feature]][instance[fclass]] += 1

	max_gain = -100
	max_gain_feature = ""
	for feature, feature_freqs in frequencies.items():
		feature_gain = gain(feature_freqs)
		if feature_gain > max_gain:
			max_gain = feature_gain
			max_gain_feature = feature

	return max_gain_feature

def id3(instances, fclass, features):
	""" Creates the decision tree and returns a <dot> format string """
	fclass_values = {}
	for instance in instances:
		if not instance[fclass] in fclass_values:
			fclass_values[instance[fclass]] = 1
		else:
			fclass_values[instance[fclass]] += 1

	if len(fclass_values) < 2:
		# Quedó un solo valor para la clase (sólo positivos o sólo negativos)
		return create_node(list(fclass_values.keys())[0])

	if not features:
		# Valor con más frecuencia para la clase
		return create_node(max(fclass_values, key = fclass_values.get))

	# Feature con mayor ganancia
	max_gain_feature = max_gain(instances, fclass, features)
	root_id = create_node(max_gain_feature)
	# Lista de features sin el de mayor ganancia
	features.remove(max_gain_feature)
	# Obtiene un conjunto con los valores distintos para el feature
	max_gain_feature_values = {instance[max_gain_feature] for instance in instances}
	# Por cada valor posible del feature
	for feature_value in max_gain_feature_values:
		# Filtrar las instancias que tienen el valor en el feature
		instances_v = [instance for instance in instances if instance[max_gain_feature] == feature_value]
		child_id = id3(instances_v, fclass, features)
		add_edge(root_id, child_id, feature_value)

	return root_id

def main(dataset_filename, fclass):
	with open(dataset_filename, "r") as dataset_file:
		features = []

		for line in dataset_file:
			start = line.upper().split(" ")[0]
			if start == "@DATA\n":
				break
			elif start == "@ATTRIBUTE":
				features.append(line.split(" ")[1])
		else:
			raise Exception("No se encontraron los datos")

		if len(features) < 3:
			raise Exception("Los features no están definidos correctamente")

		fclass = features[len(features)-1] if fclass not in features else fclass

		instances = [dict(zip(features, line.rstrip().split(','))) for line in dataset_file]

		features.remove(fclass)

	id3(instances, fclass, features)

	global tree
	nx.draw(tree, nx.spring_layout(tree), with_labels=True)
	write_dot(tree, "punto3.dot")
	print("Árbol creado en punto3.dot")
	subprocess.run(["dot", "-Tpng", "punto3.dot", "-opunto3.png"])
	print("Árbol guardado en punto3.png")

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Elabora un Decision Tree.")
	parser.add_argument("dataset", help="archivo en formato .arff con el dataset")
	parser.add_argument("-c", "--clase", default=None, help="nombre del feature a predecir (por defecto: el último declarado)")
	args = parser.parse_args()

	main(args.dataset, args.clase);