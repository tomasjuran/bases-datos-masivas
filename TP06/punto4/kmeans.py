#!/usr/bin/env python3
""" Realiza clustering con k-means """
import sys
import os
import csv
import argparse
import random

def distance(a, b, p):
	values = []
	for key, value in a.items():
		values.append((abs(a[key] - b[key]))**p)
	return sum(values)**(1/p)

def reassign_clusters(instances, clusters, centroids, p):
	new_clusters = [[] for i in range(len(clusters))]

	# Assign each instance to the cluster whose centroid is nearest
	for instance_index, instance in enumerate(instances):
		minimum_distance = distance(instance, centroids[0], p)
		# print("Instance: " + str(instance_index))
		# print("Distance to Centroid 0: " + str(minimum_distance))
		min_dis_centroid = 0
		for centroid_index, centroid in enumerate(centroids[1:]):
			current_distance = distance(instance, centroid, p)
			# print("Distance to Centroid " + str(centroid_index+1) + ": " + str(current_distance))
			if current_distance < minimum_distance:
				minimum_distance = current_distance
				min_dis_centroid = centroid_index+1 # enumerate begins at 0 but we start at index 1

		# Centroid index corresponds to cluster index
		# print("Instance " + str(instance_index) + " assigned to cluster " + str(min_dis_centroid))
		new_clusters[min_dis_centroid].append(instance_index)

	return new_clusters

def find_centroids(instances, clusters):
	columns = instances[0].keys()
	centroids = []
	# Initialize centroids
	for i in range(len(clusters)):
		centroids.append({})
		for column in columns:
			centroids[i][column] = 0
	
	# Find the centroids
	for cluster_index, cluster in enumerate(clusters):
		for instance_index in cluster:
			for column in columns:
				# Sum the values to get the mean later
				centroids[cluster_index][column] += instances[instance_index][column]

	# Get the mean
	for index, centroid in enumerate(centroids):
		for column in columns:
			centroid[column] /= max(len(clusters[index]),1)

	return centroids

def refine(instances, clusters, p):
	# print("Clusters: ")
	# print(clusters)
	centroids = find_centroids(instances, clusters)
	# print("Centroids: ")
	# print(centroids)
	new_clusters = reassign_clusters(instances, clusters, centroids, p)

	# If the new cluster definition is different, refine once more
	if new_clusters != clusters:
		return refine(instances, new_clusters, p)
	else:
		classification = []
		for cluster_index, cluster in enumerate(clusters):
			for instance_index in cluster:
				classification.append({
					"instance_index":instance_index,
					"cluster_index":cluster_index
					})
		return classification

def kmeans(instances, k, p):
	# Assign each instance to a cluster randomly
	clusters = [[] for i in range(k)]
	for index, instance in enumerate(instances):
		cluster_index = random.randint(0,k-1)
		while len(clusters[cluster_index]) > len(instances)/k:
			cluster_index = (cluster_index + 1) % k
		clusters[cluster_index].append(index)
	return refine(instances, clusters, p)

def main(dataset_filename, k, p):
	with open(dataset_filename, "r") as dataset_file:
		dictreader = csv.DictReader(dataset_file, delimiter=";")
		instances = []
		for row in dictreader:
			for key in row.keys():
				row[key] = float(row[key])
			instances.append(row)
	classification = kmeans(instances, k, p)
	output_filename = os.path.splitext(dataset_filename)[0] + "_clusters.csv"
	with open(output_filename, "w") as output_file:
		dictwriter = csv.DictWriter(output_file, fieldnames=classification[0].keys(),delimiter=";")
		dictwriter.writeheader()
		dictwriter.writerows(classification)
	print("Archivo con clasificación creado en " + output_filename)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Realiza clustering con k-means.")
	parser.add_argument("dataset", help="archivo en formato .csv con el dataset")
	parser.add_argument("-k", "--k", default=3, help="cantidad de clusters (por defecto: 3)")
	parser.add_argument("-p", "--p", default=2, help="p de Minkowski (por defecto: 2, equivalente a distancia euclidiana)")
	args = parser.parse_args()

	main(args.dataset, args.k, args.p);