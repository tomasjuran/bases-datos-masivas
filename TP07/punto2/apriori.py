#!/usr/bin/env python3
""" Genera reglas de asociación con el algoritmo Apriori """
import sys
import os
import csv
import argparse
from bisect import bisect_left, insort_left
from itertools import permutations

support_table = []

def find_in_support_table(itemset):
	global support_table
	for itemset_with_support in support_table:
		if itemset_with_support["itemset"] == itemset:
			return itemset_with_support["support"]
	return -1

def save_in_support_table(itemset, support):
	global support_table
	support_table.append({"itemset":itemset, "support":support})

# Data Mining: Concepts and Techniques, Second Edition
# by Han and Kamber,
# Ch. 5.2.1 The Apriori Algorithm: Finding Frequent Itemsets Using Candidate Generation
def get_k_plus_one_candidates(k_itemsets):
	k_plus_one_candidates = []
	for k in range(0, len(k_itemsets)):
		left_candidate = k_itemsets[k]
		
		for p in range(0, len(k_itemsets)):
			if k != p:
				right_candidate = k_itemsets[p]
				
				join_itemsets = True
				last_item_index = len(left_candidate) - 1
				
				for h in range(0, last_item_index):
					if left_candidate[h] != right_candidate[h]:
						join_itemsets = False
						break

				if join_itemsets and left_candidate[last_item_index] < right_candidate[last_item_index]:
					k_plus_one_candidates.append(
						left_candidate + [right_candidate[last_item_index]]
					)

	return k_plus_one_candidates

def get_confidence_rules(transactions, itemset):
	
	def get_one_rule(transactions, antecedent, itemset):
		consequent = [item for item in itemset if item not in antecedent]
		antecedent_support = get_support(transactions, antecedent)
		itemset_support = get_support(transactions, itemset)
		return {
				"antecedent": antecedent,
				"consequent": consequent,
				"confidence": itemset_support / antecedent_support
		}

	if len(itemset) < 2:
		return []

	confidence_rules = []
	for r in range(1, len(itemset)):
		for permutation in permutations(itemset, r):
			confidence_rules.append(
				get_one_rule(transactions, list(permutation), itemset)
			)
	return confidence_rules

def get_support(transactions, itemset):
	support = find_in_support_table(itemset)
	if support != -1:
		return support

	support_count = 0
	for transaction in transactions:
		itemset_is_present = True
	
		for item in itemset:
			if not is_in(transaction, item):
				itemset_is_present = False
	
		if itemset_is_present:
			support_count += 1

	return support_count / len(transactions)

def apriori(transactions, candidate_k_itemsets, min_sup, min_con):
	if len(candidate_k_itemsets) < 1:
		return []

	k_frequent_itemsets = []
	k_itemset_rules = []
	
	for itemset in candidate_k_itemsets:
		support = get_support(transactions, itemset)
		if (support >= min_sup):
			# Add to frequent itemsets
			k_frequent_itemsets.append(itemset)
			save_in_support_table(itemset, support)

			# Get association rules
			confidence_rules = get_confidence_rules(transactions, itemset)
			for candidate_rule in confidence_rules:
				if candidate_rule["confidence"] >= min_con:
					k_itemset_rules.append(candidate_rule)

	k_plus_one_candidate_itemsets = get_k_plus_one_candidates(k_frequent_itemsets)

	return [k_itemset_rules] + apriori(transactions, k_plus_one_candidate_itemsets, min_sup, min_con)


def main(dataset_filename, min_sup, min_con):
	transactions = []
	with open(dataset_filename, "r") as dataset_file:
		dictreader = csv.DictReader(dataset_file)
		
		first_row = next(dictreader)
		# Current transaction ID
		current_t_id = first_row["id"]
		# Current transaction items
		current_t_items = [first_row["item"]]

		# Save all 1-itemsets
		one_itemsets = []

		# Volviendo a programación 1 con los cortes de control :D
		for row in dictreader:
			if row["id"] != current_t_id:
				# Put all items in a transaction in the same list,
				# then add them to the list of transactions
				transactions.append(current_t_items)
				current_t_id = row["id"]
				current_t_items = []

			current_item = row["item"]
			# Add item to one_itemsets
			insort_if_not_found(one_itemsets, current_item)
			# Add item to transaction items
			insort_if_not_found(current_t_items, current_item)

		transactions.append(current_t_items)
		# Wrap in a list because apriori expects a list of itemsets (i.e. a list of lists)
		one_itemsets = [[x] for x in one_itemsets]

	association_rules = apriori(transactions, one_itemsets, min_sup, min_con)

	output_filename = os.path.splitext(dataset_filename)[0]
	output_filename += "-sup-" + str(min_sup).replace(".","_") 
	output_filename += "-con-" + str(min_con).replace(".","_")
	output_filename += "-rules.csv"
	with open(output_filename, "w") as output_file:
		dictwriter = csv.DictWriter(output_file, fieldnames=["antecedent", "consequent", "confidence"], delimiter=";")
		dictwriter.writeheader()

		for i in range(1, len(association_rules)-1):
			print("Reglas de asociación con ", i+1, "-itemsets:", sep="")
			for rule in association_rules[i]:
				row = {
					"antecedent": ", ".join(rule["antecedent"]),
					"consequent": ", ".join(rule["consequent"]),
					"confidence": "{0:.5f}".format(rule["confidence"])
				}
				print("{",row["antecedent"],"} -> {",row["consequent"], "}\t\tconfidence: ", row["confidence"], sep="")
				dictwriter.writerow(row)
			print()

	print("Archivo con reglas de asociación creado en " + output_filename)


def insort_if_not_found(a, x):
	if not is_in(a, x):
		insort_left(a, x)

def is_in(a, x):
	""" Returns true if x is in a, or false otherwise """
	return index_of(a, x) != -1

def index_of(a, x):
	""" Returns index of x in a, or -1 if x is not in a """
	if len(a) < 1:
		return -1
	index = bisect_left(a, x)
	# not found
	if index >= len(a) or a[index] != x:
		return -1
	return index

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Genera reglas de asociación con el algoritmo Apriori.")
	parser.add_argument("dataset", help="archivo en formato .csv con el dataset")
	parser.add_argument("-s", "--min_sup", default=0.1, help="soporte mínimo de itemset frecuente (por defecto: 0.1)")
	parser.add_argument("-c", "--min_con", default=0.8, help="confianza mínima para la regla (por defecto: 0.8)")
	args = parser.parse_args()

	main(args.dataset, float(args.min_sup), float(args.min_con));
