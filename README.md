# bases-datos-masivas

Data Mining and Machine Learning Scripts repository for "Bases de Datos Masivas" course in Universidad Nacional de Luján.

**Directories:**

TP00 - iris dataset analysis

TP03 - mongo & R integration (tweet data)

TP04 - Data preprocessing in R

TP05 - Decision Tree algorithm

TP06 - K-means clustering algorithm. Hierarchical clustering, Silhouette and Elbow method

TP07 - Association Rules: Apriori algorithm and data mining