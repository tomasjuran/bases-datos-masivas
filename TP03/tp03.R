library(mongolite)

# Crear las colecciones en la BD "dolar2"
tweets <- mongo("dolar_tweets", url = "mongodb://localhost/dolar2")
users <- mongo("dolar_users", url = "mongodb://localhost/dolar2")

# Insertar un documento
users$insert('{ 
	            "_id" : {"$oid": "5b0c115f5987f92430009553"},
              "user_id" : "117660336",
              "name" : "Juan Amorín",
              "screen_name" : "juan_amorin", 
              "location" : "Ciudad Autónoma de Buenos Aire", 
              "description" : "Periodista en @eldestapeweb | Conductor de @cronicanunciada | Colaborador en @elcoheteluna <f0><U+009F><U+009A><U+0080>. Hincha de #Banfield y devoto de @juliofalcioniDT.", 
              "url" : "https://t.co/AbfRNaZxg5", 
              "protected" : false, 
              "followers_count" : 39537, 
              "friends_count" : 735, 
              "listed_count" : 160, 
              "statuses_count" : 12822, 
              "favourites_count" : 15319, 
              "account_created_at" : {"$date": "2010-02-26T06:07:39Z"}, 
              "verified" : true, 
              "profile_url" : "https://t.co/AbfRNaZxg5", 
              "profile_expanded_url" : "https://www.facebook.com/juann.amorin", 
              "account_lang" : "es", 
              "profile_banner_url" : "https://pbs.twimg.com/profile_banners/117660336/1498623917",
              "profile_background_url" : "http://abs.twimg.com/images/themes/theme1/bg.png",
              "profile_image_url" : "http://pbs.twimg.com/profile_images/945058314047643649/VUe7eEQP_normal.jpg"
              }')

# Cambiar el atributo "name"
users$update('{"_id" : {"$oid": "5b0c115f5987f92430009553"}}',
             '{"$set": {"name" : "bdm"}}')

# Eliminar el documento
users$remove('{"_id" : {"$oid": "5b0c115f5987f92430009553"}}')

# Importar todo
users$import(file("dolar-dump_users.json"))
tweets$import(file("dolar-dump_tweets.json"))

# Proporción de Tweets con al menos una marca
tweets_con_marca <- tweets$count('{"$or": [
                                    {"retweet_count" : {"$gt" : 0}},
                                    {"favorite_count" : {"$gt" : 0}}
                                  ]}')
tweets_en_total <- tweets$count()
(tweets_con_marca / tweets_en_total) * 100

# Usuario con mayor cantidad de Tweets
tweets$aggregate('[
    {"$group": {"_id": "$user_id", "tweets": {"$sum": 1}}},
    {"$sort": {"tweets": -1}},
    {"$limit": 1}
]')


# Cantidad de usuarios que usan la aplicación Web
tweets$aggregate('[
    {"$match": {"source": "Twitter Web Client"}},
    {"$group": {"_id": "$user_id"}},
    {"$group": {"_id": 1, "tweets": {"$sum": 1}}},
    {"$project": {"_id": 0, "usuarios que usan la web":"$tweets"}}
]')


# ¿Existen cuentas cuyo lenguaje no sea español?
users$count('{"account_lang": {"$ne":"es"}}') > 0


# Usuario con mayor cantidad de seguidores
most_followers <- users$find(sort = '{"followers_count": -1}', limit = 1)

# Cantidad de marcas de los tweets del usuario con mayor cantidad de seguidores
query <- paste('[
    {"$match": {"user_id": "', most_followers["user_id"], '"}},
    {"$group": {
      "_id": "$user_id",
      "favoritos": {"$sum": "$favorite_count"},
      "retweets": {"$sum": "$retweet_count"}
      }
    }
]', sep = "")

tweets$aggregate(query)


# Relación entre cantidad de seguidores y cantidad de marcas
# Total de marcas
total_fol_mar <- users$aggregate('[
  {"$lookup":
    {
      "from": "dolar_tweets",
      "localField": "user_id",
      "foreignField": "user_id",
      "as": "tweets"
    }
  },
  {"$unwind": "$tweets"},
  {"$group": {
      "_id": "$user_id",
      "followers_count": {"$first": "$followers_count"},
      "favoritos": {"$sum": "$tweets.favorite_count"},
      "retweets": {"$sum": "$tweets.retweet_count"}
    }
  },
  {"$project": {"followers_count": "$followers_count", "total_marcas": {"$add": ["$favoritos", "$retweets"]}}},
  {"$sort": {"followers_count": 1}}
]')

# Promedio de marcas
prom_fol_mar <- users$aggregate('[
  {"$lookup":
    {
      "from": "dolar_tweets",
      "localField": "user_id",
      "foreignField": "user_id",
      "as": "tweets"
    }
  },
  {"$unwind": "$tweets"},
  {"$group": {
      "_id": "$user_id",
      "followers_count": {"$first": "$followers_count"},
      "favoritos": {"$avg": "$tweets.favorite_count"},
      "retweets": {"$avg": "$tweets.retweet_count"}
    }
  },
  {"$project": {"followers_count": "$followers_count", "total_marcas": {"$add": ["$favoritos", "$retweets"]}}},
  {"$sort": {"followers_count": 1}}
]')

# Regresión lineal
pairs(total_fol_mar[2:3], main="Dispersión: Seguidores ~ Marcas totales")
pairs(prom_fol_mar[2:3], main="Dispersión: Seguidores ~ Marcas promedio")
